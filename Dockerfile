# Set the base image (Node:14-alpine)
FROM node:14-alpine as build

# Set the work directory
WORKDIR /app

# Copy the React files to the container
COPY . /app

# Prepare the container to build React
RUN npm install
RUN npm install react-scripts@5.0.1 -g
# Build React to production version
RUN npm run build

# Prepare Nginx
FROM nginx:1.23.1-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]